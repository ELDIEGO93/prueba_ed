# SECCIÓN DE GUITARRAS ELÉCTRICAS

## Listado de nuestros modelos:

MARCAS
-----------------
- Guibson
- Fender
- Epiphone

SUS MODELOS: Los marcados están en Stock:
-------
- [x] LP Studio Faded 2016 HP WB
- [] G-400 WC
- [x] 50s 2016 HP SVSB DB

FOTOS:
------
![alt text] (http://images.gibson.com/Products/Electric-Guitars/2016/USA/Les-Paul-Studio-Faded/HP/HLPSTWBCH1_MAIN_HERO_01.jpg)
![alt text] (http://images.epiphone.com.s3.amazonaws.com/Products/SG/G-400-Faded/Gallery/POP_G400WC.jpg)
![alt text] (http://images.gibson.com/Products/Electric-Guitars/2016/USA/Les-Paul-50s-Tribute/HP/HLPST5HTGSCH3_MAIN_HERO_01.jpg)

## MODIFICACIONES
cd text
git add .

## MENSAJE
git commit -am "add files"
